const btn = document.getElementById('btn')
const footer = document.getElementById('footer')
let isTooltipOpened = false

function handleDocumentClick(event) {
  if (btn.contains(event.target)) {
    isTooltipOpened = !isTooltipOpened
  } else {
    isTooltipOpened = false
  }

  applyClasses()
}

function applyClasses() {
  if (isTooltipOpened) {
    footer.classList.add('visible')
  } else {
    footer.classList.remove('visible')
  }
}

document.addEventListener('click', handleDocumentClick)
